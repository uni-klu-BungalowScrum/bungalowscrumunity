﻿using Assets.GamePlay.Enums;
using Assets.GamePlay.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay.Abstracts
{
    /// <summary>
    /// Abstract class for the requirements to build the bungalow (cellar, first-floor, roof) and its tasks to build that
    /// Has a priority to see, how important it is to build it first and a effort how much time it will take to build all
    /// </summary>
    public abstract class AbstractAnforderung : IAnforderung
    {
        private string name;
        private PrioritätEnum priorität;
        private AufwandEnum aufwand;
        private List<Aufgabe> aufgaben;

        /// <summary>
        /// Constructur of this abstract class
        /// </summary>
        /// <param name="name">Name of the requirement for example cellar</param>
        public AbstractAnforderung(string name)
        {
            this.name = name;
            aufgaben = new List<Aufgabe>();
        }

        /// <summary>
        /// Get the name of the requirement
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }
        }

        /// <summary>
        /// Method to add a task to the requirement
        /// </summary>
        /// <param name="name">Name of the task for example cellar-north</param>
        public void AddAufgabe(string name)
        {
            aufgaben.Add(new Aufgabe(name));
        }

        /// <summary>
        /// Method to check, if all the tasks of this requirement are correctly build
        /// </summary>
        /// <returns>true: alle tasks build correctly, other case false</returns>
        public bool AufgabenGebaut()
        {
            return aufgaben.All(a => a.IstGebaut());
        }

        /// <summary>
        /// Get the effort of the requirement
        /// </summary>
        /// <returns>Effort Enum</returns>
        public AufwandEnum GetAufwand()
        {
            return aufwand;
        }

        /// <summary>
        /// Get the priority of the requirement
        /// </summary>
        /// <returns>Priority enum</returns>
        public PrioritätEnum GetPriorität()
        {
            return priorität;
        }

        /// <summary>
        /// Method sets a task to build correctly if the name exists
        /// </summary>
        /// <param name="name">Name of the task</param>
        /// <returns>true: set task to build, false: any error happens</returns>
        public bool SetAufgabeGebaut(string name)
        {
            var aufgabe = aufgaben.FirstOrDefault(f => f.Name == name);
            if (aufgabe == null)
                return false;

            aufgabe.SetAufgabeGebaut();
            return true;
        }

        /// <summary>
        /// Method sets a tasks to be build wrong
        /// </summary>
        /// <param name="name">Name of the task</param>
        /// <returns>true: set task to be build wrong, false: any error happens</returns>
        public bool SetAufgabeFehlerhaftGebaut(string name)
        {
            var aufgabe = aufgaben.FirstOrDefault(f => f.Name == name);
            if (aufgabe == null)
                return false;

            aufgabe.SetAufgabeFehlerhaftGebaut();
            return true;
        }

        /// <summary>
        /// Sets the effort of this requirement
        /// </summary>
        /// <param name="aufwand">Effort enum</param>
        public void SetAufwand(AufwandEnum aufwand)
        {
            this.aufwand = aufwand;
        }

        /// <summary>
        /// Sets the priority of this requirement
        /// </summary>
        /// <param name="priorität">priority enum</param>
        public void SetPriorität(PrioritätEnum priorität)
        {
            this.priorität = priorität;
        }

        /// <summary>
        /// Gets a task with a specific name of this requirement
        /// </summary>
        /// <param name="name">Name of the task</param>
        /// <returns>Name of the task if exists or null</returns>
        public Aufgabe GetAufgabe(string name)
        {
            return aufgaben.FirstOrDefault(f => f.Name == name);
        }
    }
}
