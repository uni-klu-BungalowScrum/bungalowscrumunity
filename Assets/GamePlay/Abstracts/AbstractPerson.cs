﻿using Assets.GamePlay.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.GamePlay.Enums;

namespace Assets.GamePlay.Abstracts
{
    /// <summary>
    /// Abstract class for the persons of the scrum team.
    /// Every Person has a name, gets a role in the team and has its experiences of this role.
    /// </summary>
    public abstract class AbstractPerson : IPerson
    {
        private string name;
        private IDictionary<RolleEnum, ErfahrungEnum> erfahrungen;
        private RolleEnum rolle;

        /// <summary>
        /// Constructor of this class which takes as parameter the name of the person
        /// </summary>
        /// <param name="name">Name of the Person</param>
        public AbstractPerson(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// If the person is set to a role of the team, this method returns the experience of this role from the person
        /// </summary>
        /// <returns>Role of the team</returns>
        public ErfahrungEnum GetErfahrung()
        {
            if (rolle == RolleEnum.Nothing)
                throw new Exception("Rolle from Person not set");
            return erfahrungen[rolle];
        }

        /// <summary>
        /// This method returns any experince of any role which the person can have
        /// </summary>
        /// <param name="rolle">Role of the team</param>
        /// <returns>Experience of the person</returns>
        public ErfahrungEnum GetErfahrung(RolleEnum rolle)
        {
            return erfahrungen[rolle];
        }

        /// <summary>
        /// Adds roles an their experiences of the person of the team
        /// </summary>
        /// <param name="rolle">Role which can be taken in the team</param>
        /// <param name="erfahrung">Experience of a specific role</param>
        public void AddErfahrung(RolleEnum rolle, ErfahrungEnum erfahrung)
        {
            if (erfahrungen == null)
                erfahrungen = new Dictionary<RolleEnum, ErfahrungEnum>();
            erfahrungen.Add(rolle, erfahrung);
        }

        /// <summary>
        /// Sets the role that her/she takes in the team
        /// </summary>
        /// <param name="rolle">Role in the team</param>
        public void SetRolle(RolleEnum rolle)
        {
            this.rolle = rolle;
        }

        /// <summary>
        /// Returns the role of the person
        /// </summary>
        /// <returns>null if it has no role</returns>
        public RolleEnum GetRolle()
        {
            return rolle;
        }

        /// <summary>
        /// Property to get the name of the person
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }
        }
    }
}
