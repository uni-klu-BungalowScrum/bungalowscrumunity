﻿using Assets.GamePlay.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay.Anforderungen
{
    /// <summary>
    /// Requirement roof. One element of three to build the bungalow
    /// </summary>
    public class Dach : AbstractAnforderung
    {
        /// <summary>
        /// Constructor which adds one task to the requirement and sets the name of the requirement
        /// </summary>
        public Dach() : base("Dach")
        {
            AddAufgabe("Dach");
        }
    }
}
