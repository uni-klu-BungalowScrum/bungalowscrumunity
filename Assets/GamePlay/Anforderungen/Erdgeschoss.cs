﻿using Assets.GamePlay.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay.Anforderungen
{
    /// <summary>
    /// Requirement first-floor. One element of three to build the bungalow
    /// </summary>
    public class Erdgeschoss : AbstractAnforderung
    {
        /// <summary>
        /// Constructor which adds three tasks to the requirement and sets the name of the requirement
        /// </summary>
        public Erdgeschoss() : base("Erdgeschoss")
        {
            AddAufgabe("Erdgeschosswand Norden+Osten");
            AddAufgabe("Erdgeschosswand Süden+Westen");
            AddAufgabe("Erdgeschossdecke");
        }
    }
}
