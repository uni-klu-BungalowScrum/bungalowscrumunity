﻿using Assets.GamePlay.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay.Anforderungen
{
    /// <summary>
    /// Requirement cellar. One element of three to build the bungalow
    /// </summary>
    public class Keller : AbstractAnforderung
    {
        /// <summary>
        /// Constructor which adds five tasks to the requirement and sets the name of the requirement
        /// </summary>
        public Keller() : base("Keller")
        {
            AddAufgabe("Kellerwand Norden");
            AddAufgabe("Kellerwand Osten");
            AddAufgabe("Kellerwand Süden");
            AddAufgabe("Kellerwand Westen");
            AddAufgabe("Kellerdecke");
        }
    }
}
