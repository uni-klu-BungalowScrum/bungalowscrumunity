﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay
{
    /// <summary>
    /// Class Task, which has to be build for a requirement to build the bungalow
    /// It has a name and the properties, if its already build and if it is build correctly
    /// </summary>
    public class Aufgabe
    {
        private string name;
        private bool gebaut;
        private bool fehlerhaftGebaut;

        /// <summary>
        /// Constructor of the class, which sets the name of the task and set the booleans build and buildfaulty to false
        /// </summary>
        /// <param name="name">Name of the task</param>
        public Aufgabe(string name)
        {
            this.name = name;
            gebaut = false;
            fehlerhaftGebaut = false;
        }

        /// <summary>
        /// To get the name of the task
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }
        }

        /// <summary>
        /// Method to look up, if the task is build correctly
        /// </summary>
        /// <returns>true: task is build correctly, false: not build correctly</returns>
        public bool IstGebaut()
        {
            return gebaut;
        }

        /// <summary>
        /// Method to look up, if the task is build faulty
        /// </summary>
        /// <returns>true: task is build faulty, false: not build faulty</returns>
        public bool IstFehlerhaftGebaut()
        {
            return fehlerhaftGebaut;
        }

        /// <summary>
        /// Sets the task to build correctly
        /// </summary>
        public void SetAufgabeGebaut()
        {
            gebaut = true;
            fehlerhaftGebaut = false;
        }

        /// <summary>
        /// Sets the task to build faulty
        /// </summary>
        public void SetAufgabeFehlerhaftGebaut()
        {
            gebaut = false;
            fehlerhaftGebaut = true;
        }
    }
}
