﻿using Assets.GamePlay;
using Assets.GamePlay.Abstracts;
using Assets.GamePlay.Anforderungen;
using Assets.GamePlay.Enums;
using Assets.GamePlay.Persons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay
{
    /// <summary>
    /// Main class to build up the bungalow in this game.
    /// Its a singleton, that every script i used can access it
    /// </summary>
    public class BuildBungalow
    {
        /// <summary>
        /// Constant names of all the task, which has to be build and for the robot
        /// </summary>
        #region constants
        public const string ROBOT_ANIMATOR_STEP = "Robot_Step";
        public const string KELLER_NORDEN = "Kellerwand Norden";
        public const string KELLER_OSTEN = "Kellerwand Osten";
        public const string KELLER_SÜDEN = "Kellerwand Süden";
        public const string KELLER_WESTEN = "Kellerwand Westen";
        public const string KELLERDECKE = "Kellerdecke";
        public const string ERDGESCHOSS_NO = "Erdgeschosswand Norden+Osten";
        public const string ERDGESCHOSS_SW = "Erdgeschosswand Süden+Westen";
        public const string ERDGESCHOSSDECKE = "Erdgeschossdecke";
        public const string DACH = "Dach";
        #endregion

        private bool isGameStarted = false;
        private RobotSteps robotStep = RobotSteps.Default;
        private bool mussZumRoboterGehn = false;
        private GameSteps gameStep = GameSteps.Default;
        private Schwierigkeitslevel level = Schwierigkeitslevel.Leicht;

        private Sprintplansitzung sprintPlanung = null;
        private SprintAblauf sprintAblauf = null;
        private ProjektEnde projektEnde = null;

        /// <summary>
        /// Initialization of the singleton
        /// </summary>
        #region initialize

        private static BuildBungalow instance;

        private BuildBungalow() { }

        public static BuildBungalow Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BuildBungalow();
                }
                return instance;
            }
        }
        #endregion

        #region GameStarted
        /// <summary>
        /// Starts the bungalow game and sets the properties to the startvalue
        /// </summary>
        public void StartGame() { isGameStarted = true; robotStep = RobotSteps.Start; gameStep = GameSteps.Sprintplansitzung; InitialiseDummy(); }

        /// <summary>
        /// Ends the game und resets all the values to default (start value)
        /// </summary>
        public void EndGame() { ResetGame(); }

        /// <summary>
        /// Method to see, if the game is started or not
        /// </summary>
        /// <returns>true: starte, false: not started</returns>
        public bool IsGameStarted() { return isGameStarted; }
        #endregion

        #region GamePlay

        /// <summary>
        /// Sets the robot to sprintplan session, the gamestep to organize the team and initialize the class sprintplanning
        /// </summary>
        public void StarteSprintplansitzung()
        {
            robotStep = RobotSteps.Sprintplansitzung;
            gameStep = GameSteps.TeamEinteilen;
            sprintPlanung = new Sprintplansitzung();
        }

        /// <summary>
        /// Sets the robot to the sprint beginning on the field, the gamestep to dailyscrum meeting and initalize the sprint process and starts the sprint
        /// </summary>
        public void StarteSprint()
        {
            robotStep = RobotSteps.Sprint;
            gameStep = GameSteps.DailyScrumMeeting;
            if (sprintAblauf == null)
                sprintAblauf = new SprintAblauf(sprintPlanung);
            sprintAblauf.StarteSprint();
        }

        /// <summary>
        /// Call this method if every task and requirement of a sprint has be done.
        /// Sets the robot to sprintend and the gameStep to buildend.
        /// </summary>
        public void AnforderungenVonSprintGebaut()
        {
            robotStep = RobotSteps.SprintEnde;
            gameStep = GameSteps.BauenEnde;
        }

        /// <summary>
        /// Call this method if the sprintreview has to be done and the sprint is over
        /// </summary>
        public void SprintEnde()
        {
            if (sprintAblauf.Sprint == 1)
                robotStep = RobotSteps.Sprintplansitzung;
            else
                robotStep = RobotSteps.ProjektEnde;
            gameStep = GameSteps.SprintEnde;
            
            sprintAblauf.BeendeSprint();
        }

        /// <summary>
        /// Call this method the product is finished and a review meeting and so on for the product has to be done
        /// </summary>
        public void ProjektEnde()
        {
            projektEnde = new ProjektEnde();
            gameStep = GameSteps.ProjektEndeSitzung;
            robotStep = RobotSteps.ProjektEnde;
        }

        /// <summary>
        /// Call this method, if the product is handed out to the customer
        /// </summary>
        public void GameEnde()
        {
            robotStep = RobotSteps.Ende;
            gameStep = GameSteps.Bungalow;
        }

        #endregion

        #region ResetGame
        /// <summary>
        /// Every game property is set to default
        /// </summary>
        private void ResetGame()
        {
            gameStep = GameSteps.Default;
            robotStep = RobotSteps.Default;
            isGameStarted = false;
            sprintPlanung = null;
            sprintAblauf = null;
            projektEnde = null;
        }
        #endregion

        #region Properties

        public Sprintplansitzung GetSprintplanSitzung () { return sprintPlanung; }
        public SprintAblauf GetSprintAblauf() { return sprintAblauf; }
        public ProjektEnde GetProjektEnde() { return projektEnde; }
        public RobotSteps GetRobotStep() { return robotStep; }
        public GameSteps GetGameStep() { return gameStep; }
        public void SetGameStep(GameSteps step)
        {
            gameStep = step;
            switch(step)
            {
                case GameSteps.SprintErstellen:
                case GameSteps.SprintBacklogInAufgabenTeilenUndZuweisen:
                    mussZumRoboterGehn = true;
                    break;
            }
        }
        public Schwierigkeitslevel GetSchwierigkeitsLevel() { return level; }
        public bool MussZumRoboter
        {
            get
            {
                return mussZumRoboterGehn;
            }
            set
            {
                mussZumRoboterGehn = value;
            }
        }
        #endregion

        internal void AllePersonenZugeteilt()
        {
            if (sprintPlanung.Axel_Position > 0 && sprintPlanung.Bernd_Position > 0 && sprintPlanung.Christine_Position > 0 && sprintPlanung.Stefanie_Position > 0)
                gameStep = GameSteps.ProduktBacklogKategorisieren;
        }

        /// <summary>
        /// Dummy initialization, to test some things of a specific point at the game, that you don't have to do all the things every time before.
        /// </summary>
        private void InitialiseDummy()
        {
            //gameStep = GameSteps.BauenEnde;
            //sprintPlanung = new Sprintplansitzung();
            //AbstractPerson axel = new Axel();
            //axel.SetRolle(RolleEnum.ProductOwner);
            //AbstractPerson bernd = new Bernd();
            //bernd.SetRolle(RolleEnum.ScrumMaster);
            //AbstractPerson christine = new Christine();
            //christine.SetRolle(RolleEnum.TeamBauen);
            //AbstractPerson stefanie = new Stefanie();
            //stefanie.SetRolle(RolleEnum.TeamKorrigieren);
            //sprintPlanung.SetPerson(axel);
            //sprintPlanung.SetPerson(bernd);
            //sprintPlanung.SetPerson(christine);
            //sprintPlanung.SetPerson(stefanie);
            //sprintPlanung.PriorisiereBacklogAnforderung(typeof(Keller), PrioritätEnum.Hoch);
            //sprintPlanung.PriorisiereBacklogAnforderung(typeof(Erdgeschoss), PrioritätEnum.Mittel);
            //sprintPlanung.PriorisiereBacklogAnforderung(typeof(Dach), PrioritätEnum.Wenig);
            //sprintPlanung.SchatzeAufwandVonAnforderung(typeof(Keller), AufwandEnum.SehrHoch);
            //sprintPlanung.SchatzeAufwandVonAnforderung(typeof(Erdgeschoss), AufwandEnum.Mittel);
            //sprintPlanung.SchatzeAufwandVonAnforderung(typeof(Dach), AufwandEnum.SehrWenig);
            //sprintPlanung.AddAnforderungToSprint("Sprint_1", sprintPlanung.ProductBacklog.GetAnforderung(typeof(Keller)));
            //sprintPlanung.AddAnforderungToSprint("Sprint_2", sprintPlanung.ProductBacklog.GetAnforderung(typeof(Erdgeschoss)));
            //sprintPlanung.AddAnforderungToSprint("Sprint_2", sprintPlanung.ProductBacklog.GetAnforderung(typeof(Dach)));

            //StarteSprint();
            //sprintAblauf = GetSprintAblauf();
            //sprintAblauf.DailyScrumMeeting();
            //sprintAblauf.BaueAnforderung(typeof(Keller), KELLER_NORDEN);
            //sprintAblauf.DailyScrumMeeting();
            //sprintAblauf.BaueAnforderung(typeof(Keller), KELLER_OSTEN);
            //sprintAblauf.DailyScrumMeeting();
            //sprintAblauf.BaueAnforderung(typeof(Keller), KELLER_SÜDEN);
            //sprintAblauf.DailyScrumMeeting();
            //sprintAblauf.BaueAnforderung(typeof(Keller), KELLER_WESTEN);
            //sprintAblauf.DailyScrumMeeting();
            //sprintAblauf.BaueAnforderung(typeof(Keller), KELLERDECKE);
            //if (sprintAblauf.CurrentSprint.AnforderungenGebaut())
            //{
            //    AnforderungenVonSprintGebaut();
            //}
            //var sprint = sprintAblauf.CurrentSprint;
            //sprint.ReviewMeetingDurchgeführt();
            //sprint.ProduktinkrementÜberprüft(ÜberprüfungsEnum.Akzeptiert);
            //sprint.AkzeptanttestDurchgeführt(ÜberprüfungsEnum.Akzeptiert);
            //SprintEnde();

            //StarteSprint();
            //sprintAblauf.DailyScrumMeeting();
            //sprintAblauf.BaueAnforderung(typeof(Erdgeschoss), ERDGESCHOSS_NO);
            //sprintAblauf.DailyScrumMeeting();
            //sprintAblauf.BaueAnforderung(typeof(Erdgeschoss), ERDGESCHOSS_SW);
            //sprintAblauf.DailyScrumMeeting();
            //sprintAblauf.BaueAnforderung(typeof(Erdgeschoss), ERDGESCHOSSDECKE);
            //sprintAblauf.DailyScrumMeeting();
            //sprintAblauf.BaueAnforderung(typeof(Dach), DACH);
            //if (sprintAblauf.CurrentSprint.AnforderungenGebaut())
            //{
            //    AnforderungenVonSprintGebaut();
            //}
            //sprint = sprintAblauf.CurrentSprint;
            //sprint.ReviewMeetingDurchgeführt();
            //sprint.ProduktinkrementÜberprüft(ÜberprüfungsEnum.Akzeptiert);
            //sprint.AkzeptanttestDurchgeführt(ÜberprüfungsEnum.Akzeptiert);
            //SprintEnde();
        }
    }
}
