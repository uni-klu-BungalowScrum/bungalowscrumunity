﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay.Enums
{
    /// <summary>
    /// Enum for the requirement, how much effort it will take in days
    /// </summary>
    public enum AufwandEnum
    {
        Default = 0,
        SehrWenig = 1,
        Wenig = 2,
        Mittel = 3,
        Hoch = 4,
        SehrHoch = 5
    }
}
