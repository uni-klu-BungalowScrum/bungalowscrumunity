﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay.Enums
{
    /// <summary>
    /// Enum for the experience of the person for any role
    /// </summary>
    public enum ErfahrungEnum
    {
        SehrSchlecht,
        Schlecht,
        Mittel,
        Gut,
        SehrGut
    }
}
