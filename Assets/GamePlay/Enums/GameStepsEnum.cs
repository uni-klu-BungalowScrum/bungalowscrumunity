﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay.Enums
{
    /// <summary>
    /// Enum for the game to know, how far the player is in the game
    /// </summary>
    public enum GameSteps
    {
        Default = -1,
        Sprintplansitzung = 0,
        TeamEinteilen = 1,
        ProduktBacklogKategorisieren = 2,
        AufwandSchätzen = 3,
        SprintErstellen = 4,
        SprintBacklogInAufgabenTeilenUndZuweisen = 5,
        SprintplansitzungEnde = 6,
        DailyScrumMeeting = 7,
        Bauen = 8,
        Korrigieren = 9,
        BauenEnde = 10,
        ReviewMeeting = 11,
        ProduktinkrementÜberprüfen = 12,
        AkzeptanztestMitKunden = 13,
        SprintEnde = 14,
        ProjektEndeSitzung = 15,
        Dokumentation = 16,
        ProduktÜbergabe = 17,
        Bungalow = 18
    }
}
