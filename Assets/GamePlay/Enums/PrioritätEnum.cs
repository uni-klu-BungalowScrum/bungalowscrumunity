﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay.Enums
{
    /// <summary>
    /// Enum for the priority of the requirements to build the bungalow
    /// </summary>
    public enum PrioritätEnum
    {
        Default = 0,
        Wenig = 1,
        Mittel = 2,
        Hoch = 3
    }
}
