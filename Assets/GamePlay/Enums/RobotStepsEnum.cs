﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay.Enums
{
    /// <summary>
    /// Enum, for the robot in the game, to set its position and get so the correct text to display
    /// </summary>
    public enum RobotSteps
    {
        Default = -1,
        Start = 0,
        Sprintplansitzung = 1,
        Sprint = 2,
        SprintEnde = 3,
        ProjektEnde = 4,
        Ende = 5
    }
}
