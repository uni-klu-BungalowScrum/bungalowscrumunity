﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay.Enums
{
    /// <summary>
    /// Enum for the roles, which the persons can take
    /// </summary>
    public enum RolleEnum
    {
        Nothing = 0,
        ProductOwner = 1,
        ScrumMaster = 2,
        TeamBauen = 3,
        TeamKorrigieren = 4
    }
}
