﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay.Enums
{
    /// <summary>
    /// Maybe for later on, to have different difficulty levels
    /// </summary>
    public enum Schwierigkeitslevel
    {
        Leicht,
        Mittel,
        Schwer
    }
}
