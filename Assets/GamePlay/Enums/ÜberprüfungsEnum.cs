﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay.Enums
{
    /// <summary>
    /// Enum, if a produkt increment is reviewed and if it is accepted
    /// </summary>
    public enum ÜberprüfungsEnum
    {
        NichtÜberprüft,
        Akzeptiert,
        NichtAkzeptiert
    }
}
