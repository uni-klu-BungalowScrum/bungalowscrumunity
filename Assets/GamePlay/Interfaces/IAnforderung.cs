﻿using Assets.GamePlay.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay.Interfaces
{
    /// <summary>
    /// Interface for the requirement to provide for every the same methods
    /// </summary>
    public interface IAnforderung
    {
        AufwandEnum GetAufwand();
        void SetAufwand(AufwandEnum aufwand);
        PrioritätEnum GetPriorität();
        void SetPriorität(PrioritätEnum priorität);
        void AddAufgabe(string name);
        Aufgabe GetAufgabe(string name);
        bool AufgabenGebaut();
        bool SetAufgabeGebaut(string name);
        bool SetAufgabeFehlerhaftGebaut(string name);
    }
}
