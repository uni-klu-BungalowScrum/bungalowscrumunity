﻿using Assets.GamePlay.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay.Interfaces
{
    /// <summary>
    /// Interface for the person to provide for all the same methods
    /// </summary>
    public interface IPerson
    {
        ErfahrungEnum GetErfahrung(RolleEnum rolle);
        void AddErfahrung(RolleEnum rolle, ErfahrungEnum erfahrung);
        void SetRolle(RolleEnum rolle);
        RolleEnum GetRolle();
    }
}
