﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay
{
    /// <summary>
    /// Class which has a list of the messages for the robot in the game, which helps you throw the game
    /// </summary>
    public class MessagesForRobot
    {
        private List<string> messagesLeicht;

        /// <summary>
        /// Constructor to initalize messages
        /// </summary>
        public MessagesForRobot()
        {
            Initialize();
        }

        /// <summary>
        /// Messages for the robot
        /// </summary>
        private void Initialize()
        {
            messagesLeicht = new List<string>();
            messagesLeicht.Add("'e': Sprintplanung starten");
            messagesLeicht.Add("Finden Sie einen Tisch an den Personen sitzen und teilen sie diese in Rollen ein");
            messagesLeicht.Add("Gehen Sie zum Product Owner und lassen sie den Product Backlog (Artefakt!) priorisieren");
            messagesLeicht.Add("Gehen Sie in den Meeting Raum, um mit den Product Owner und den Team den Aufwand zu schätzen");
            messagesLeicht.Add("Gehen Sie in den Meeting Raum, um mit den Product Owner und den Team den die zwei Sprints zusammen zu stellen");
            messagesLeicht.Add("Gehen Sie in den Meeting Raum, um mit den Product Owner und den Team den Sprint Backlog (Artefakt!) zu zerteilen und dem Team zuzuweisen");
            messagesLeicht.Add("Sprintplanung ist zu Ende!!\n'e': Sprint starten");
            messagesLeicht.Add("Ab zum DailyScrum Meeting!");
            messagesLeicht.Add("Gehen Sie zum Team und bauen Sie die nächste Anforderung");
            messagesLeicht.Add("Es gab einen Fehler, gehen Sie zum Team und korrigieren Sie diesen");
            messagesLeicht.Add("Anforderungen wurden gebaut!");
            messagesLeicht.Add("Gehen Sie in den Meeting Raum, um mit allen ein Sprint Review durchzuführen");
            messagesLeicht.Add("Produktinkrement muss überprüft werden. Gehen Sie zum Product Owner");
            messagesLeicht.Add("Akzeptanztest mit den Kunden, er müsste jederzeit kommen");
            messagesLeicht.Add("Der Sprint ist zu Ende");
            messagesLeicht.Add("Gehen Sie in den Meeting Raum, um mit allen eine Projekt Ende Sitzung durchzuführen");
            messagesLeicht.Add("Gehen Sie zum Team, damit es die Dokumentation über das Projekt schreibt");
            messagesLeicht.Add("Der ProductOwner übergibt nun das fertige Produkt den Kunden");
            messagesLeicht.Add("Bungalow FERTIG!!! :)");
        }

        /// <summary>
        /// Depending on the difficulty of the game and the game step <paramref name="gameStep"/> (is set in the singleton class BuildBungalow), it returns the message
        /// </summary>
        /// <returns>Gets the Message for the robot for the current situation</returns>
        public string GetMessage()
        {
            if (BuildBungalow.Instance.GetSchwierigkeitsLevel() == Enums.Schwierigkeitslevel.Leicht)
                return messagesLeicht[(int)BuildBungalow.Instance.GetGameStep()];

            return "";
        }
    }
}
