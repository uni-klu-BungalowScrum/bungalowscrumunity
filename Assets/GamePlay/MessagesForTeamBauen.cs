﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay
{
    /// <summary>
    /// Class which has a list of the messages for the person in the role TeamBauen
    /// </summary>
    public class MessagesForTeamBauen
    {
        private List<string> messagesLeicht;

        /// <summary>
        /// Constructor to initalize messages
        /// </summary>
        public MessagesForTeamBauen()
        {
            Initialize();
        }

        /// <summary>
        /// Messages for the Role TeamBauen
        /// </summary>
        private void Initialize()
        {
            messagesLeicht = new List<string>();
            messagesLeicht.Add("Drück Sie 'e' um die Kellerwand Norden zu bauen");
            messagesLeicht.Add("Drück Sie 'e' um die Kellerwand Osten zu bauen");
            messagesLeicht.Add("Drück Sie 'e' um die Kellerwand Süden zu bauen");
            messagesLeicht.Add("Drück Sie 'e' um die Kellerwand Westen zu bauen");
            messagesLeicht.Add("Drück Sie 'e' um die Kellerdecke zu bauen");
            messagesLeicht.Add("Drück Sie 'e' um die Erdgeschosswande Norden+Osten zu bauen");
            messagesLeicht.Add("Drück Sie 'e' um die Erdgeschosswände Süden+Westen zu bauen");
            messagesLeicht.Add("Drück Sie 'e' um die Erdgeschosswand zu bauen");
            messagesLeicht.Add("Drück Sie 'e' um die Dach zu bauen");
        }

        /// <summary>
        /// Depending on the difficulty of the game and the the task, which has to build up next, it returns the message
        /// </summary>
        /// <returns>Gets the Message for the role TeamBauen for the current situation</returns>
        public string GetMessage(int idx)
        {
            if (BuildBungalow.Instance.GetSchwierigkeitsLevel() == Enums.Schwierigkeitslevel.Leicht)
                return messagesLeicht[idx];
            return "";
        }
    }
}
