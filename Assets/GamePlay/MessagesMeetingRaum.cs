﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay
{
    /// <summary>
    /// Class which has a list of the messages for the meeting room in the game
    /// </summary>
    public class MessagesMeetingRaum
    {
        private List<string> messagesLeicht;

        /// <summary>
        /// Constructor to initalize messages
        /// </summary>
        public MessagesMeetingRaum()
        {
            Initialize();
        }

        /// <summary>
        /// Messages for the robot
        /// </summary>
        private void Initialize()
        {
            messagesLeicht = new List<string>();
            messagesLeicht.Add("Drücken Sie 'e' um Aufwandsschätzung zu erhalten");
            messagesLeicht.Add("Drücken Sie 'e' um Sprint in Aufgaben zu zerteilen");
            messagesLeicht.Add("Drücken Sie 'e' um die Aufgaben den Team zuzuweisen");
            messagesLeicht.Add("Drücken Sie 'e' um den Fragen zu antworten");
            messagesLeicht.Add("Drücken Sie 'e' um Review zu starten");
            messagesLeicht.Add("Drücken Sie 'e' um die Sitzung für das Projektende zu starten");
        }

        /// <summary>
        /// Depending on the difficulty of the game and the index, it returns the message for the meeting room
        /// </summary>
        /// <param name="idx">Index for the message</param>
        /// <returns>Message for the room</returns>
        public string GetMessage(int idx)
        {
            if (BuildBungalow.Instance.GetSchwierigkeitsLevel() == Enums.Schwierigkeitslevel.Leicht)
                return messagesLeicht[idx];
            return "";
        }
    }
}
