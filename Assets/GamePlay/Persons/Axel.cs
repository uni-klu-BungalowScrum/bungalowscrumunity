﻿using Assets.GamePlay.Abstracts;
using Assets.GamePlay.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay.Persons
{
    /// <summary>
    /// Person Axel which is predefined with his experiences in his roles
    /// </summary>
    public class Axel : AbstractPerson
    {
        /// <summary>
        /// Constructur of the person to set the name ane the experiences
        /// </summary>
        public Axel() : base("Axel")
        {
            AddErfahrung(RolleEnum.ProductOwner, ErfahrungEnum.SehrGut);
            AddErfahrung(RolleEnum.ScrumMaster, ErfahrungEnum.Gut);
            AddErfahrung(RolleEnum.TeamBauen, ErfahrungEnum.Schlecht);
            AddErfahrung(RolleEnum.TeamKorrigieren, ErfahrungEnum.Mittel);
        }
    }
}
