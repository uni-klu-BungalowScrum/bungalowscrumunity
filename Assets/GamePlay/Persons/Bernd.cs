﻿using Assets.GamePlay.Abstracts;
using Assets.GamePlay.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay.Persons
{
    /// <summary>
    /// Person Bernd which is predefined with his experiences in his roles
    /// </summary>
    public class Bernd : AbstractPerson
    {
        /// <summary>
        /// Constructur of the person to set the name ane the experiences
        /// </summary>
        public Bernd() : base("Bernd")
        {
            AddErfahrung(RolleEnum.ProductOwner, ErfahrungEnum.Mittel);
            AddErfahrung(RolleEnum.ScrumMaster, ErfahrungEnum.SehrGut);
            AddErfahrung(RolleEnum.TeamBauen, ErfahrungEnum.Gut);
            AddErfahrung(RolleEnum.TeamKorrigieren, ErfahrungEnum.Schlecht);
        }
    }
}
