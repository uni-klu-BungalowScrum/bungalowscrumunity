﻿using Assets.GamePlay.Abstracts;
using Assets.GamePlay.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay.Persons
{
    /// <summary>
    /// Person Christine which is predefined with her experiences in his roles
    /// </summary>
    public class Christine : AbstractPerson
    {
        /// <summary>
        /// Constructur of the person to set the name ane the experiences
        /// </summary>
        public Christine() : base("Christine")
        {
            AddErfahrung(RolleEnum.ProductOwner, ErfahrungEnum.SehrSchlecht);
            AddErfahrung(RolleEnum.ScrumMaster, ErfahrungEnum.Mittel);
            AddErfahrung(RolleEnum.TeamBauen, ErfahrungEnum.SehrGut);
            AddErfahrung(RolleEnum.TeamKorrigieren, ErfahrungEnum.Gut);
        }
    }
}
