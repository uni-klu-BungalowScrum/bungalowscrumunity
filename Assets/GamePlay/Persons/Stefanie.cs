﻿using Assets.GamePlay.Abstracts;
using Assets.GamePlay.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay.Persons
{
    /// <summary>
    /// Person Stefanie which is predefined with her experiences in his roles
    /// </summary>
    public class Stefanie : AbstractPerson
    {
        /// <summary>
        /// Constructur of the person to set the name ane the experiences
        /// </summary>
        public Stefanie() : base("Stefanie")
        {
            AddErfahrung(RolleEnum.ProductOwner, ErfahrungEnum.Mittel);
            AddErfahrung(RolleEnum.ScrumMaster, ErfahrungEnum.Schlecht);
            AddErfahrung(RolleEnum.TeamBauen, ErfahrungEnum.SehrGut);
            AddErfahrung(RolleEnum.TeamKorrigieren, ErfahrungEnum.SehrGut);
        }
    }
}
