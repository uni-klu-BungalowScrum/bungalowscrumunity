﻿using Assets.GamePlay.Abstracts;
using Assets.GamePlay.Anforderungen;
using Assets.GamePlay.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay
{
    /// <summary>
    /// Class to store the requirements to build up the bungalow
    /// </summary>
    public class ProductBacklog
    {
        private List<AbstractAnforderung> anforderungen; 

        /// <summary>
        /// Constructor which initialize the list of requirements with the cellar, first-flor and roof
        /// </summary>
        public ProductBacklog()
        {
            anforderungen = new List<AbstractAnforderung>
            {
                new Dach(),
                new Keller(),
                new Erdgeschoss()
            };
        }

        /// <summary>
        /// Method to prioritize a requirement
        /// </summary>
        /// <param name="type">Type of the requirement</param>
        /// <param name="priorität">Priority</param>
        /// <returns>true: done succesfull, false: any error happens</returns>
        public bool PriorisiereAnforderung(Type type, PrioritätEnum priorität)
        {
            var anforderung = anforderungen.FirstOrDefault(f => f.GetType() == type);
            if (anforderung == null)
                return false;

            anforderung.SetPriorität(priorität);
            return true;
        }

        /// <summary>
        /// Metho to estimate the effort of the requirement
        /// </summary>
        /// <param name="type">Type of the requirement</param>
        /// <param name="aufwand">Effort</param>
        /// <returns>true: done succesfull, false: any error happens</returns>
        public bool SetAufwand(Type type, AufwandEnum aufwand)
        {
            var anforderung = anforderungen.FirstOrDefault(f => f.GetType() == type);
            if (anforderung == null)
                return false;

            anforderung.SetAufwand(aufwand);
            return true;
        }

        /// <summary>
        /// Method to get all requirements
        /// </summary>
        /// <returns>List of requirements</returns>
        public List<AbstractAnforderung> GetAnforderungen()
        {
            return anforderungen;
        }

        /// <summary>
        /// Method to get a specific requirement
        /// </summary>
        /// <param name="type">Type of the requirement</param>
        /// <returns>Requirement</returns>
        public AbstractAnforderung GetAnforderung(Type type)
        {
            return anforderungen.FirstOrDefault(f => f.GetType() == type);
        }
    }
}
