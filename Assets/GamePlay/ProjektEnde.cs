﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay
{
    /// <summary>
    /// Class for the Projectend, which is needed if every Requirement is build and every Sprint is already done
    /// </summary>
    public class ProjektEnde
    {
        private bool sitzungDurchgeführt = false;
        private bool dokumentationGeschrieben = false;
        private bool produktÜbergeben = false;

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public ProjektEnde()
        {
        }

        /// <summary>
        /// Sets the boolean, that a review meeting at the end was done
        /// </summary>
        public void SetSitzungDurchgeführt()
        {
            sitzungDurchgeführt = true;
        }

        /// <summary>
        /// Sets the boolean, that a dokumentation of the whole progress was written
        /// </summary>
        public void SetDokumentationGeschrieben()
        {
            dokumentationGeschrieben = true;
        }

        /// <summary>
        /// Sets the boolean, that the Product(Bungalow) was handed over the customer
        /// </summary>
        public void SetProduktFertigÜbergeben()
        {
            produktÜbergeben = true;
        }

        /// <summary>
        /// Method to see, if the review meeting was done or not
        /// </summary>
        /// <returns>true: was done, false: was not done</returns>
        public bool GetProjektEndeSitzungDurchgeführt() { return sitzungDurchgeführt; }

        /// <summary>
        /// Method to see, if the dokumentation was written or not
        /// </summary>
        /// <returns>true: written, false: not written</returns>
        public bool GetDokumentationGescrhieben() { return dokumentationGeschrieben; }

        /// <summary>
        /// Method to see, if the product was handed out to the customer
        /// </summary>
        /// <returns>true: handed out, false: not handed out</returns>
        public bool GetProduktÜbergeben() { return produktÜbergeben; }
    }
}
