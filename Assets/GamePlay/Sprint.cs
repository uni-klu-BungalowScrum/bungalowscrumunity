﻿using Assets.GamePlay.Abstracts;
using Assets.GamePlay.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay
{
    /// <summary>
    /// Class Sprint, has a name and number of days it has and stores the requirements which has do be done in this sprint.
    /// It also stores the class SprintEnde, because there is always a review after each sprint
    /// </summary>
    public class Sprint
    {
        private string name;
        private int anzahlTage;
        private List<AbstractAnforderung> anforderungen;
        private SprintEnde sprintEnde;

        /// <summary>
        /// Constructor of the class. It initialize the list of the requirements and sets the name and the number of days of the sprint
        /// </summary>
        /// <param name="name">SprintName</param>
        /// <param name="anzahlTage">Number of days</param>
        public Sprint(string name, int anzahlTage)
        {
            this.name = name;
            this.anzahlTage = anzahlTage;
            anforderungen = new List<AbstractAnforderung>();
        }

        /// <summary>
        /// This method adds a Requirement to the sprint.
        /// The call of the method and the check, if it can be added at all, is in the class SprintBacklog
        /// </summary>
        /// <param name="anforderung"></param>
        public void AddAnforderung(AbstractAnforderung anforderung)
        {
            anforderungen.Add(anforderung);
        }

        /// <summary>
        /// To Get the name of the Sprint
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }
        }

        /// <summary>
        /// Method which returns all the Requirements of this sprints
        /// </summary>
        /// <returns>List of requirements</returns>
        public List<AbstractAnforderung> GetAnforderungen()
        {
            return anforderungen;
        }

        /// <summary>
        /// Method returns a specific requirement
        /// </summary>
        /// <param name="type">Type of the requirement</param>
        /// <returns>Requirement</returns>
        public AbstractAnforderung GetAnforderung(Type type)
        {
            return anforderungen.FirstOrDefault(f => f.GetType() == type);
        }

        /// <summary>
        /// Method to returns the count of the days, which the sprint will take. In our case always 5
        /// </summary>
        /// <returns>Number of days of the sprint</returns>
        public int GetAnzahlTage()
        {
            return anzahlTage;
        }

        /// <summary>
        /// Method to look up, if all the requirements with all its tasks a correctly build
        /// </summary>
        /// <returns>true: all correctly build, false: not all correctly build</returns>
        public bool AnforderungenGebaut()
        {
            foreach (var anforderung in anforderungen)
            {
                if (!anforderung.AufgabenGebaut())
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Method to set at the sprint end, that a review meeting was done
        /// </summary>
        public void ReviewMeetingDurchgeführt()
        {
            CheckSprintEndeInitialised();
            sprintEnde.SprintReviewDurchgeführt();
        }

        /// <summary>
        /// Method to set at the sprint end, that the productincrement was approved from the product owner
        /// </summary>
        public void ProduktinkrementÜberprüft(ÜberprüfungsEnum ergebnis)
        {
            CheckSprintEndeInitialised();
            sprintEnde.ProduktInkrementÜberprüft(ergebnis);
        }

        /// <summary>
        /// Method to set at the sprint end, that the productincrement wos accepted by the customer
        /// </summary>
        public void AkzeptanttestDurchgeführt(ÜberprüfungsEnum ergebnis)
        {
            CheckSprintEndeInitialised();
            sprintEnde.AkzeptanztTestDurchgeführt(ergebnis);
        }

        /// <summary>
        /// Checks if the class "SprintEnde" is initialised
        /// </summary>
        private void CheckSprintEndeInitialised()
        {
            if (sprintEnde == null)
                sprintEnde = new SprintEnde();
        }

        /// <summary>
        /// To get the class SprintEnde
        /// </summary>
        public SprintEnde SprintEnde
        {
            get
            {
                return sprintEnde;
            }
        }
    }
}
