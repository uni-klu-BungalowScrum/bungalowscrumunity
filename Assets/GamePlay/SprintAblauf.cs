﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay
{
    /// <summary>
    /// Class which handles the to sprints, which has to be done to build up the bungalow
    /// </summary>
    public class SprintAblauf
    {
        private Sprintplansitzung sprintPlanSitzung;
        private int tag = 0;
        private Sprint currentSprint;
        private int sprint = 0;

        /// <summary>
        /// Constructor of the class
        /// </summary>
        /// <param name="sprintPlanSitzung">Class Sprintplansitzung</param>
        public SprintAblauf(Sprintplansitzung sprintPlanSitzung)
        {
            this.sprintPlanSitzung = sprintPlanSitzung;
        }

        /// <summary>
        /// At the first call this method sets the first sprint from the sprintplan session to the "currentsprint" here
        /// At the second call it will set the second sprint
        /// </summary>
        public void StarteSprint()
        {
            sprint++;
            currentSprint = sprintPlanSitzung.GetSprint("Sprint_" + sprint);
        }

        /// <summary>
        /// Increase the count of the days
        /// </summary>
        public void DailyScrumMeeting()
        {
            tag++;
        }

        /// <summary>
        /// Method to build a task of a requirement correctly
        /// </summary>
        /// <param name="anforderungsTyp">Type of the requirement</param>
        /// <param name="aufgabe">Taskname</param>
        /// <returns>true: succesfully build, false: requirement not exists</returns>
        public bool BaueAnforderung(Type anforderungsTyp, string aufgabe)
        {
            var anforderung = currentSprint.GetAnforderung(anforderungsTyp);
            if (anforderung == null)
                return false;

            anforderung.SetAufgabeGebaut(aufgabe);
            return true;
        }

        /// <summary>
        /// Method to build a task of a requirement faulty
        /// </summary>
        /// <param name="anforderungsTyp">Type of the requirement</param>
        /// <param name="aufgabe">Taskname</param>
        /// <returns>true: succesfully build faulty, false: requirement not exists</returns>
        public bool BaueAnforderungFehlerhaft(Type anforderungsTyp, string aufgabe)
        {
            var anforderung = currentSprint.GetAnforderung(anforderungsTyp);
            if (anforderung == null)
                return false;

            anforderung.SetAufgabeFehlerhaftGebaut(aufgabe);
            return true;
        }

        /// <summary>
        /// Method to correct a task, which was build faulty
        /// </summary>
        /// <param name="anforderungsTyp">Type of the requirement</param>
        /// <param name="aufgabe">Taskname</param>
        /// <returns>true: successfully corrected, false: requirement not exists</returns>
        public bool KorrigiereAnforderung(Type anforderungsTyp, string aufgabe)
        {
            var anforderung = currentSprint.GetAnforderung(anforderungsTyp);
            if (anforderung == null)
                return false;

            anforderung.SetAufgabeGebaut(aufgabe);
            return true;
        }

        /// <summary>
        /// Sets the currentSprint to null
        /// </summary>
        public void BeendeSprint()
        {
            currentSprint = null;
        }


        /// <summary>
        /// To get the current sprint
        /// </summary>
        public Sprint CurrentSprint
        {
            get
            {
                return currentSprint;
            }
        }

        /// <summary>
        /// To get the current day of the whole project
        /// </summary>
        public int Tag
        {
            get
            {
                return tag;
            }
        }

        /// <summary>
        /// To get the current sprint, either 1 or 2
        /// </summary>
        public int Sprint
        {
            get
            {
                return sprint;
            }
        }

        /// <summary>
        /// Method to see, if all the tasks of every requirement and sprint are correctly build
        /// </summary>
        /// <returns>true: productincrement finished, false: productincrement has to be build</returns>
        public bool SindSprintsFertig()
        {
            foreach (var sprint in sprintPlanSitzung.GetSprints())
            {
                if (sprint.AnforderungenGebaut() == false)
                    return false;
            }
            return true;
        }
    }
}
