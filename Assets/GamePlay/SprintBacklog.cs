﻿using Assets.GamePlay.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay
{
    /// <summary>
    /// Class SprintBacklog which stores the two sprints with its requirements
    /// </summary>
    public class SprintBacklog
    {
        private List<Sprint> sprints;

        /// <summary>
        /// Constructor which initialize the sprint list
        /// </summary>
        public SprintBacklog()
        {
            sprints = new List<Sprint>();
        }

        /// <summary>
        /// Method to add a sprint to the list
        /// </summary>
        /// <param name="name">Sprintname</param>
        /// <param name="anzahlTage">Number of days it take</param>
        public void AddSprint(string name, int anzahlTage)
        {
            sprints.Add(new Sprint(name, anzahlTage));
        }

        /// <summary>
        /// Method to add a Requirement to one of the two sprints
        /// It also checks, if the requirement with this affort takes place at this sprint
        /// </summary>
        /// <param name="sprintName">Sprintname</param>
        /// <param name="anforderung">Requirement class</param>
        /// <returns>true: adds successfully the requirement to the sprint, false: any error happens</returns>
        public bool AddAnforderungToSprint(string sprintName, AbstractAnforderung anforderung)
        {
            var sprint = sprints.FirstOrDefault(f => f.Name == sprintName);
            if (sprint == null)
                return false;

            var tage = 0;
            foreach (var anforderungen in sprint.GetAnforderungen())
            {
                tage += (int)anforderung.GetAufwand();
            }
            if ((tage + (int)anforderung.GetAufwand()) > sprint.GetAnzahlTage())
                return false;

            sprint.AddAnforderung(anforderung);
            return true;
        }

        /// <summary>
        /// Method to get a specific sprint
        /// </summary>
        /// <param name="name">Sprintname</param>
        /// <returns>Sprint class</returns>
        public Sprint GetSprint(string name)
        {
            return sprints.FirstOrDefault(f => f.Name == name);
        }

        /// <summary>
        /// Method to get both sprints
        /// </summary>
        /// <returns>List of sprints</returns>
        public List<Sprint> GetSprints()
        {
            return sprints;
        }
    }
}
