﻿using Assets.GamePlay.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay
{
    /// <summary>
    /// Class which is needed at the end of every sprint
    /// </summary>
    public class SprintEnde
    {
        private bool sprintReviewDurchgeführt = false;
        private ÜberprüfungsEnum produktInkrement;
        private ÜberprüfungsEnum akzeptanzTest;

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public SprintEnde()
        {
        }

        /// <summary>
        /// Sets, that a sprint review at the end was done
        /// </summary>
        public void SprintReviewDurchgeführt()
        {
            sprintReviewDurchgeführt = true;
        }

        /// <summary>
        /// Method to set, if the product increment was approved or not
        /// </summary>
        /// <param name="ergebnis">Enum, if it was accepted or not</param>
        public void ProduktInkrementÜberprüft(ÜberprüfungsEnum ergebnis)
        {
            if (ergebnis == ÜberprüfungsEnum.NichtÜberprüft)
                throw new Exception(ergebnis.ToString() + " ist kein gültiger Wert");
            produktInkrement = ergebnis;
        }

        /// <summary>
        /// Method to set, if the customer checked the product increment and is pleased with it or not
        /// </summary>
        /// <param name="ergebnis">Enum, if it was accepted or not</param>
        public void AkzeptanztTestDurchgeführt(ÜberprüfungsEnum ergebnis)
        {
            if (ergebnis == ÜberprüfungsEnum.NichtÜberprüft)
                throw new Exception(ergebnis.ToString() + " ist kein gültiger Wert");
            akzeptanzTest = ergebnis;
        }

        /// <summary>
        /// To see, if the review meeting was done or not
        /// </summary>
        /// <returns>true: done, false: not done</returns>
        public bool IstReviewDurchgeführt() { return sprintReviewDurchgeführt; }

        /// <summary>
        /// To get the value of the product increment check by the productowner
        /// </summary>
        /// <returns>Enum: Not Checked, accepted, not accpted</returns>
        public ÜberprüfungsEnum GetProduktInkrementÜberprüfung() { return produktInkrement; }

        /// <summary>
        /// To get the value of the product increment check by the customer
        /// </summary>
        /// <returns>Enum: Not Checked, accepted, not accpted</returns>
        public ÜberprüfungsEnum GetAkzeptanztestÜberprüfung() { return akzeptanzTest; }
    }
}
