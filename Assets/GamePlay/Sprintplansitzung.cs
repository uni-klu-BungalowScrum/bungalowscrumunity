﻿using Assets.GamePlay.Abstracts;
using Assets.GamePlay.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GamePlay
{
    /// <summary>
    /// This class includes all of the things, which happens in the sprintplan session.
    /// 1. Set Person(s) to the role(s)
    /// 2. Sets the productbacklog with the requirements, prioritize them and estimate the effort
    /// 3. Split the requirements to the two sprints
    /// </summary>
    public class Sprintplansitzung
    {
        public int Axel_Position = 0;
        public int Bernd_Position = 0;
        public int Christine_Position = 0;
        public int Stefanie_Position = 0;

        private AbstractPerson productOwner;
        private AbstractPerson scrumMaster;
        private AbstractPerson teamBauen;
        private AbstractPerson teamKorrigieren;

        private ProductBacklog productBacklog;
        private SprintBacklog sprintBacklog;

        /// <summary>
        /// Constructor which initalizes the Product Backlog and the Sprint Backlog and adds two sprints with 5 days time to the sprintbacklog
        /// </summary>
        public Sprintplansitzung()
        {
            productBacklog = new ProductBacklog();
            sprintBacklog = new SprintBacklog();
            AddSprint("Sprint_1", 5);
            AddSprint("Sprint_2", 5);
        }

        /// <summary>
        /// Method to distribute the persons to roles of the team
        /// </summary>
        /// <param name="person">Person, that should be assigned as a prerequisite for a role</param>
        public void SetPerson(AbstractPerson person)
        {
            if (person.GetRolle() == RolleEnum.ProductOwner)
                productOwner = person;
            else if (person.GetRolle() == RolleEnum.ScrumMaster)
                scrumMaster = person;
            else if (person.GetRolle() == RolleEnum.TeamBauen)
                teamBauen = person;
            else if (person.GetRolle() == RolleEnum.TeamKorrigieren)
                teamKorrigieren = person;
        }

        /// <summary>
        /// Method to look up, if a specific person is already set to a role
        /// </summary>
        /// <param name="name">Person name</param>
        /// <returns>true: is set, false: is not set</returns>
        public bool PersonAlreadySet(string name)
        {
            if (productOwner != null && productOwner.Name == name)
                return true;
            if (scrumMaster != null && scrumMaster.Name == name)
                return true;
            if (teamBauen != null && teamBauen.Name == name)
                return true;
            if (teamKorrigieren != null && teamKorrigieren.Name == name)
                return true;
            return false;
        }

        /// <summary>
        /// Method to priorize a requirement
        /// </summary>
        /// <param name="type">Type of the requirement f.e. (Keller)</param>
        /// <param name="priorität">Priority of the requiremnt</param>
        public void PriorisiereBacklogAnforderung(Type type, PrioritätEnum priorität)
        {
            productBacklog.PriorisiereAnforderung(type, priorität);
        }

        /// <summary>
        /// Method to estimate the effort of the requirement
        /// </summary>
        /// <param name="type">Type of the requirement f.e. (Keller)</param>
        /// <param name="aufwand">Effort of the requirement</param>
        public void SchatzeAufwandVonAnforderung(Type type, AufwandEnum aufwand)
        {
            productBacklog.SetAufwand(type, aufwand);
        }

        /// <summary>
        /// Method to adds a sprint to the sprintbacklog
        /// </summary>
        /// <param name="name">Name of the sprint, either Sprint_1 or Sprint_2</param>
        /// <param name="anzahlTage">Count of the days of the sprint, in my case always 5</param>
        private void AddSprint(string name, int anzahlTage)
        {
            sprintBacklog.AddSprint(name, anzahlTage);
        }

        /// <summary>
        /// Method to gets a sprint
        /// </summary>
        /// <param name="name">Sprintname</param>
        /// <returns>Sprint class</returns>
        public Sprint GetSprint(string name)
        {
            return sprintBacklog.GetSprint(name);
        }

        /// <summary>
        /// Method to get both sprints
        /// </summary>
        /// <returns>List of sprint classes</returns>
        public List<Sprint> GetSprints()
        {
            return sprintBacklog.GetSprints();
        }

        /// <summary>
        /// Method to add a requirement to a sprint
        /// </summary>
        /// <param name="sprintName">Name of the Sprint</param>
        /// <param name="anforderung">Class of a requirement</param>
        /// <returns>true: successfully added, false: any error happens</returns>
        public bool AddAnforderungToSprint(string sprintName, AbstractAnforderung anforderung)
        {
            return sprintBacklog.AddAnforderungToSprint(sprintName, anforderung);
        }

        /// <summary>
        /// Returns the class ProductBacklog
        /// </summary>
        public ProductBacklog ProductBacklog
        {
            get { return productBacklog; }
        }

        /// <summary>
        /// Returns the class SprintBacklog
        /// </summary>
        public SprintBacklog SprintBacklog
        {
            get { return sprintBacklog; }
        }

        /// <summary>
        /// Returns the Person of the role ProductOwner
        /// </summary>
        public AbstractPerson ProductOwner
        {
            get{ return productOwner; }
        }

        /// <summary>
        /// Returns the Person of the role ScrumMaster
        /// </summary>
        public AbstractPerson ScrumMuser
        { 
            get{ return scrumMaster; }
        }

        /// <summary>
        /// Returns the Person of the role TeamBauen
        /// </summary>
        public AbstractPerson TeamBauen
        {
            get { return teamBauen; }
        }

        /// <summary>
        /// Returns the Person of the role TeamKorrigieren
        /// </summary>
        public AbstractPerson TeamKorrigieren
        {
            get { return teamKorrigieren; }
        }
    }
}
