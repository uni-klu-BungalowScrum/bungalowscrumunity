﻿using Assets.GamePlay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bungalow : MonoBehaviour {

    public GameObject panel = null;
    private Text text = null;
    private bool isOnTrigger = false;
	// Use this for initialization
	void Start () {
        panel.SetActive(false);
        text = panel.transform.GetComponentInChildren<Text>();
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            isOnTrigger = true;
            panel.SetActive(true);
            if (BuildBungalow.Instance.GetGameStep() == Assets.GamePlay.Enums.GameSteps.ProduktinkrementÜberprüfen)
            {
                text.text = "Das Produktinkrement sieht gut aus. Es kann den Kunden gezeigt werden";
                BuildBungalow.Instance.GetSprintAblauf().CurrentSprint.ProduktinkrementÜberprüft(Assets.GamePlay.Enums.ÜberprüfungsEnum.Akzeptiert);
            }
            else if (BuildBungalow.Instance.GetGameStep() == Assets.GamePlay.Enums.GameSteps.AkzeptanztestMitKunden && !BuildBungalow.Instance.MussZumRoboter)
            {
                text.text = "Ich bin sehr zufrieden mit den Sprintergebnis und den Produktinkrement ;-) !!!";
                BuildBungalow.Instance.GetSprintAblauf().CurrentSprint.AkzeptanttestDurchgeführt(Assets.GamePlay.Enums.ÜberprüfungsEnum.Akzeptiert);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (BuildBungalow.Instance.GetGameStep() == Assets.GamePlay.Enums.GameSteps.ProduktinkrementÜberprüfen)
            {
                var anim = GameObject.FindGameObjectWithTag("ProductOwner").GetComponent<Animator>();
                anim.SetBool("ProduktInkrementCheck", false);
                BuildBungalow.Instance.SetGameStep(Assets.GamePlay.Enums.GameSteps.AkzeptanztestMitKunden);
                BuildBungalow.Instance.MussZumRoboter = true;
            }
            else if (BuildBungalow.Instance.GetGameStep() == Assets.GamePlay.Enums.GameSteps.AkzeptanztestMitKunden && !BuildBungalow.Instance.MussZumRoboter)
            {
                var anim = GameObject.FindGameObjectWithTag("Kunde").GetComponent<Animator>();
                anim.SetBool("KundeKommt", false);
                BuildBungalow.Instance.SprintEnde();
            }
            isOnTrigger = false;
            panel.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update () {
		if (isOnTrigger)
        {
        }
	}
}
