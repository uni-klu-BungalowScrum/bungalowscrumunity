﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class MainDoorAnimation : MonoBehaviour
    {
        private Animator anim;
        public GameObject panel = null;
        private bool isOnTrigger = false;
        private Text text = null;
        private bool isDoorOpen = false;
        private void Start()
        {
            anim = GetComponent<Animator>();
            panel.SetActive(false);
            text = panel.transform.GetChild(0).GetComponentInChildren<Text>();
        }

        private void OnTriggerEnter(Collider other)
        {
            isDoorOpen = anim.GetBool("open");
            if (other.tag == "Player")
            {
                if (isDoorOpen)
                    text.text = "'e': Tür schließen";
                else
                    text.text = "'e': Tür öffnen";
                isOnTrigger = true;
                panel.SetActive(true);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.tag == "Player")
            {
                isOnTrigger = false;
                panel.SetActive(false);
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.E) && isOnTrigger)
            {
                if (isDoorOpen)
                {
                    anim.SetBool("open", false);
                }
                else
                {
                    anim.SetBool("open", true);
                }
            }
        }
    }
}
