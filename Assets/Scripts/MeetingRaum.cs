﻿using Assets.GamePlay;
using Assets.GamePlay.Anforderungen;
using Assets.GamePlay.Enums;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MeetingRaum : MonoBehaviour {
    public Material aufwandSchätzung = null;
    public Material sprintBacklogHelp = null;
    public Material sprintBacklog_leer = null;
    public Material sprintBacklog_K1 = null;
    public Material sprintBacklog_K1E2 = null;
    public Material sprintBacklog_K1E2D2 = null;
    public Material sprintBacklog_K2 = null;
    public Material sprintBacklog_K2E1 = null;
    public Material sprintBacklog_K2E1D1 = null;
    public Material sprintBacklogErstellt = null;
    public Material kellerAufgaben = null;
    public Material kellerAufgabenErledigt = null;
    public Material erdgeschossDachAufgaben = null;
    public Material erdgeschossDachAufgabenErledigt = null;
    public Material dailyScrumFragen = null;
    public List<Material> dailyScrumAntworten = null;
    public List<Material> reviews = null;
    public Material projektEndeSitzung = null;

    public GameObject panel = null;
    private Renderer boardRenderer = null;
    private GameObject meetingHelp = null;
    private bool isOnTrigger = false;
    private Text text = null;
    private int messageIndex = 0;
    private MessagesMeetingRaum meetingMessages = null;
    private bool setKellerOn1 = false;
    private int anzahlBacklogGesetzt = 0;
    private Sprintplansitzung sitzung = null;
    private bool sprintInAufgabenZerteilt = false;
    private bool hatAufwandGeschätzt = false;
    private bool hatSprintAufgabenZugeteilt = false;
    private bool dailyScrumMeetingFertig = false;
    private bool setDailyScrumMeeting = false;
    private SprintAblauf sprintAblauf = null;
    private bool reviewFertig = false;
    private bool projektEndeSitzungFertig = false;

    private Animator productOwnerAnim = null;
    private Animator scrumMasterAnim = null;
    private Animator teamBauenAnim = null;
    private Animator teamKorrigieren = null;

    // Use this for initialization
    void Start () {
        boardRenderer = GameObject.FindGameObjectWithTag("MeetingBoard").GetComponentInChildren<Renderer>();
        meetingHelp = GameObject.FindGameObjectWithTag("MeetingHelp");
        meetingHelp.SetActive(false);
        panel.SetActive(false);
        text = panel.transform.GetChild(0).GetComponentInChildren<Text>();
        meetingMessages = new MessagesMeetingRaum();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            SetTeamMemberAnimators();

            if (BuildBungalow.Instance.GetGameStep() == GameSteps.AufwandSchätzen)
            {
                panel.SetActive(true);
                
            }
            else if (BuildBungalow.Instance.GetGameStep() == GameSteps.SprintErstellen && anzahlBacklogGesetzt == 0 && BuildBungalow.Instance.MussZumRoboter == false)
            {
                meetingHelp.SetActive(true);
                meetingHelp.GetComponentInChildren<Renderer>().sharedMaterial = sprintBacklogHelp;
                boardRenderer.sharedMaterial = sprintBacklog_leer;
            }
            else if (BuildBungalow.Instance.GetGameStep() == GameSteps.SprintBacklogInAufgabenTeilenUndZuweisen && BuildBungalow.Instance.MussZumRoboter == false)
            {
                messageIndex = 1;
                panel.SetActive(true);
                if (setKellerOn1)
                    boardRenderer.sharedMaterial = sprintBacklog_K1E2D2;
                else
                    boardRenderer.sharedMaterial = sprintBacklog_K2E1D1;
            }
            else if (BuildBungalow.Instance.GetGameStep() == GameSteps.DailyScrumMeeting && BuildBungalow.Instance.MussZumRoboter == false)
            {
                messageIndex = 3;
                panel.SetActive(true);
                meetingHelp.SetActive(true);
                sprintAblauf = BuildBungalow.Instance.GetSprintAblauf();
                if (setDailyScrumMeeting == false)
                {
                    setDailyScrumMeeting = true;
                    sprintAblauf.DailyScrumMeeting();
                }
                boardRenderer.sharedMaterial = dailyScrumFragen;
            }
            else if (BuildBungalow.Instance.GetGameStep() == GameSteps.ReviewMeeting || BuildBungalow.Instance.GetGameStep() == GameSteps.ProjektEndeSitzung)
            {
                panel.SetActive(true);
                messageIndex++;
            }

            SetText();
            isOnTrigger = true;
        }
    }

    private void SetTeamMemberAnimators()
    {
        productOwnerAnim = GameObject.FindGameObjectWithTag("ProductOwner").GetComponentInChildren<Animator>();
        scrumMasterAnim = GameObject.FindGameObjectWithTag("ScrumMaster").GetComponentInChildren<Animator>();
        teamBauenAnim = GameObject.FindGameObjectWithTag("TeamBauen").GetComponentInChildren<Animator>();
        teamKorrigieren = GameObject.FindGameObjectWithTag("TeamKorrigieren").GetComponentInChildren<Animator>();
    }

    private void SetText()
    {
        text.text = meetingMessages.GetMessage(messageIndex);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            panel.SetActive(false);
            isOnTrigger = false;
            if (BuildBungalow.Instance.GetGameStep() == GameSteps.AufwandSchätzen && hatAufwandGeschätzt)
            {
                PersonenVerlassenMeetingRaum();
                BuildBungalow.Instance.SetGameStep(GameSteps.SprintErstellen);
            }
            else if (BuildBungalow.Instance.GetGameStep() == GameSteps.SprintErstellen && anzahlBacklogGesetzt == 3)
            {
                PersonenVerlassenMeetingRaum();
                BuildBungalow.Instance.SetGameStep(GameSteps.SprintBacklogInAufgabenTeilenUndZuweisen);
            }
            else if (BuildBungalow.Instance.GetGameStep() == GameSteps.SprintBacklogInAufgabenTeilenUndZuweisen && hatSprintAufgabenZugeteilt)
            {
                PersonenVerlassenMeetingRaum();
                hatSprintAufgabenZugeteilt = false;
                BuildBungalow.Instance.SetGameStep(GameSteps.SprintplansitzungEnde);
            }
            else if (BuildBungalow.Instance.GetGameStep() == GameSteps.DailyScrumMeeting && dailyScrumMeetingFertig)
            {
                PersonenVerlassenMeetingRaum();
                setDailyScrumMeeting = false;
                dailyScrumMeetingFertig = false;
                BuildBungalow.Instance.SetGameStep(GameSteps.Bauen);
            }
            else if (BuildBungalow.Instance.GetGameStep() == GameSteps.ReviewMeeting && reviewFertig)
            {
                reviewFertig = false;
                PersonenVerlassenMeetingRaum();
                BuildBungalow.Instance.SetGameStep(GameSteps.ProduktinkrementÜberprüfen);
            }
            else if (BuildBungalow.Instance.GetGameStep() == GameSteps.ProjektEndeSitzung && projektEndeSitzungFertig)
            {
                PersonenVerlassenMeetingRaum();
                BuildBungalow.Instance.SetGameStep(GameSteps.Dokumentation);
            }
        }
    }

    private void PersonenVerlassenMeetingRaum()
    {
        productOwnerAnim.SetBool("ToMeetingRaum", false);
        scrumMasterAnim.SetBool("ToMeetingRaum", false);
        teamBauenAnim.SetBool("ToMeetingRaum", false);
        teamKorrigieren.SetBool("ToMeetingRaum", false);
    }

    // Update is called once per frame
    void Update () {
		if (isOnTrigger)
        {
            if (BuildBungalow.Instance.GetGameStep() == GameSteps.AufwandSchätzen && Input.GetKeyDown(KeyCode.E) && hatAufwandGeschätzt == false)
            {
                boardRenderer.sharedMaterial = aufwandSchätzung;
                sitzung = BuildBungalow.Instance.GetSprintplanSitzung();
                sitzung.SchatzeAufwandVonAnforderung(typeof(Keller), AufwandEnum.SehrHoch);
                sitzung.SchatzeAufwandVonAnforderung(typeof(Erdgeschoss), AufwandEnum.Mittel);
                sitzung.SchatzeAufwandVonAnforderung(typeof(Dach), AufwandEnum.SehrWenig);
                hatAufwandGeschätzt = true;
                panel.SetActive(false);
                messageIndex++;
            }
            else if (BuildBungalow.Instance.GetGameStep() == GameSteps.SprintErstellen && anzahlBacklogGesetzt < 3)
            {
                SetSprintBacklog();
            }
            else if (BuildBungalow.Instance.GetGameStep() == GameSteps.SprintBacklogInAufgabenTeilenUndZuweisen && Input.GetKeyDown(KeyCode.E) && hatSprintAufgabenZugeteilt == false)
            {
                SprintBacklogInAufgabenZerteilen();
            }
            else if (BuildBungalow.Instance.GetGameStep() == GameSteps.DailyScrumMeeting && Input.GetKeyDown(KeyCode.E) && dailyScrumMeetingFertig == false)
            {
                meetingHelp.GetComponentInChildren<Renderer>().sharedMaterial = dailyScrumAntworten[sprintAblauf.Tag - 1];
                dailyScrumMeetingFertig = true;
                panel.SetActive(false);
            }
            else if (BuildBungalow.Instance.GetGameStep() == GameSteps.ReviewMeeting && Input.GetKeyDown(KeyCode.E) && reviewFertig == false)
            {
                meetingHelp.SetActive(true);
                if (sprintAblauf.Tag <= 5)
                    meetingHelp.GetComponentInChildren<Renderer>().sharedMaterial = reviews[0];
                else
                    meetingHelp.GetComponentInChildren<Renderer>().sharedMaterial = reviews[1];
                panel.SetActive(false);
                reviewFertig = true;
                sprintAblauf.CurrentSprint.ReviewMeetingDurchgeführt();
            }
            else if (BuildBungalow.Instance.GetGameStep() == GameSteps.ProjektEndeSitzung && Input.GetKeyDown(KeyCode.E) && projektEndeSitzungFertig == false)
            {
                meetingHelp.SetActive(true);
                meetingHelp.GetComponentInChildren<Renderer>().sharedMaterial = projektEndeSitzung;
                panel.SetActive(false);
                projektEndeSitzungFertig = true;
                BuildBungalow.Instance.GetProjektEnde().SetSitzungDurchgeführt();
            }
        }
	}

    private void SprintBacklogInAufgabenZerteilen()
    {
        if (sprintInAufgabenZerteilt)
        {
            if (BuildBungalow.Instance.GetSprintAblauf() == null)
                meetingHelp.GetComponentInChildren<Renderer>().sharedMaterial = kellerAufgabenErledigt;
            else
                meetingHelp.GetComponentInChildren<Renderer>().sharedMaterial = erdgeschossDachAufgabenErledigt;
            sprintInAufgabenZerteilt = false;
            hatSprintAufgabenZugeteilt = true;
            messageIndex++;
            panel.SetActive(false);
        }
        else
        {
            if (BuildBungalow.Instance.GetSprintAblauf() == null)
                meetingHelp.GetComponentInChildren<Renderer>().sharedMaterial = kellerAufgaben;
            else
                meetingHelp.GetComponentInChildren<Renderer>().sharedMaterial = erdgeschossDachAufgaben;
            sprintInAufgabenZerteilt = true;
            productOwnerAnim.SetBool("ToMeetingRaum", false);
            messageIndex++;
            SetText();
        }
    }

    private void SetSprintBacklog()
    {
        sitzung = BuildBungalow.Instance.GetSprintplanSitzung();
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (anzahlBacklogGesetzt == 0)
            {
                setKellerOn1 = true;
                if (sitzung.AddAnforderungToSprint("Sprint_1", sitzung.ProductBacklog.GetAnforderung(typeof(Keller))))
                {
                    boardRenderer.sharedMaterial = sprintBacklog_K1;
                    anzahlBacklogGesetzt++;
                }
            }
            else if (anzahlBacklogGesetzt == 1)
            {
                if (sitzung.AddAnforderungToSprint("Sprint_1", sitzung.ProductBacklog.GetAnforderung(typeof(Erdgeschoss))))
                {
                    boardRenderer.sharedMaterial = sprintBacklog_K2E1;
                    anzahlBacklogGesetzt++;
                }
            }
            else
            {
                if (sitzung.AddAnforderungToSprint("Sprint_1", sitzung.ProductBacklog.GetAnforderung(typeof(Dach))))
                {
                    boardRenderer.sharedMaterial = sprintBacklog_K2E1D1;
                    anzahlBacklogGesetzt++;
                }
            }
        }
        else if (Input.GetKeyDown(KeyCode.R))
        {
            if (anzahlBacklogGesetzt == 0)
            {
                setKellerOn1 = false;
                if (sitzung.AddAnforderungToSprint("Sprint_2", sitzung.ProductBacklog.GetAnforderung(typeof(Keller))))
                {
                    boardRenderer.sharedMaterial = sprintBacklog_K2;
                    anzahlBacklogGesetzt++;
                }
            }
            else if (anzahlBacklogGesetzt == 1)
            {
                if (sitzung.AddAnforderungToSprint("Sprint_2", sitzung.ProductBacklog.GetAnforderung(typeof(Erdgeschoss))))
                {
                    boardRenderer.sharedMaterial = sprintBacklog_K1E2;
                    anzahlBacklogGesetzt++;
                }
            }
            else
            {
                if (sitzung.AddAnforderungToSprint("Sprint_2", sitzung.ProductBacklog.GetAnforderung(typeof(Dach))))
                {
                    boardRenderer.sharedMaterial = sprintBacklog_K1E2D2;
                    anzahlBacklogGesetzt++;
                }
            }
        }
        if (anzahlBacklogGesetzt == 3)
        {
            meetingHelp.GetComponentInChildren<Renderer>().sharedMaterial = sprintBacklogErstellt;
        }
    }
}
