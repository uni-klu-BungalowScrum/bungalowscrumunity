﻿using Assets.GamePlay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProduchBacklogPriorisieren : MonoBehaviour {
    public Animator dachAnim = null;
    private int dachPrio = 1;

    private bool isOnTrigger = false;
	// Use this for initialization
	void Start () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            isOnTrigger = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            isOnTrigger = false;
        }
    }

    // Update is called once per frame
    void Update () {
		if (isOnTrigger)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (dachPrio <= 2)
                {
                    dachPrio++;
                    dachAnim.SetInteger("DachPrio", dachPrio);
                }
            }
            if (Input.GetKeyDown(KeyCode.R))
            {
                if (dachPrio >= 2)
                {
                    dachPrio--;
                    dachAnim.SetInteger("DachPrio", dachPrio);
                }
            }
            if (Input.GetKeyDown(KeyCode.F))
            {
                BuildBungalow.Instance.SetGameStep(Assets.GamePlay.Enums.GameSteps.AufwandSchätzen); 
            }
        }
	}
}
