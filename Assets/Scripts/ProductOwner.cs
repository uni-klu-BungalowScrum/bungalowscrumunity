﻿using Assets.GamePlay;
using Assets.GamePlay.Anforderungen;
using Assets.GamePlay.Enums;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProductOwner : MonoBehaviour {
    private Animator anim = null;
    public GameObject panel = null;
    public Texture defaultTexture = null;
    public Texture productBacklog = null;
    private bool isOnTrigger = false;
    private RawImage rawImage = null;
    private Text text = null;

    // Use this for initialization
    void Start () {
        panel.SetActive(false);
        text = panel.transform.GetChild(0).GetComponentInChildren<Text>();
        rawImage = panel.GetComponentInChildren<RawImage>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (BuildBungalow.Instance.GetGameStep() == GameSteps.ProduktBacklogKategorisieren)
            { 
                rawImage.texture = productBacklog;
                isOnTrigger = true;
                panel.SetActive(true);

                var sprintPlanug = BuildBungalow.Instance.GetSprintplanSitzung();
                sprintPlanug.PriorisiereBacklogAnforderung(typeof(Keller), PrioritätEnum.Hoch);
                sprintPlanug.PriorisiereBacklogAnforderung(typeof(Erdgeschoss), PrioritätEnum.Mittel);
                sprintPlanug.PriorisiereBacklogAnforderung(typeof(Dach), PrioritätEnum.Wenig);
                BuildBungalow.Instance.SetGameStep(GameSteps.AufwandSchätzen);
            }
            else if (BuildBungalow.Instance.GetGameStep() == GameSteps.ProduktinkrementÜberprüfen)
            {
                panel.SetActive(true);
                rawImage.texture = null;
                text.text = "Drücken Sie 'e' um das Produkt Inkrement (Artefakt!) mit den Product Owner zu überprüfen";
                isOnTrigger = true;
            }
        }
    }

    private void SetText()
    {
        rawImage.texture = defaultTexture;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            isOnTrigger = false;
            panel.SetActive(false);
        }
    }
    // Update is called once per frame
    void Update () {
		if (isOnTrigger)
        {
            if (BuildBungalow.Instance.GetGameStep() == GameSteps.ProduktinkrementÜberprüfen && Input.GetKeyDown(KeyCode.E))
            {
                var productOwner = GameObject.FindGameObjectWithTag("ProductOwner");
                anim = productOwner.GetComponent<Animator>();
                anim.SetBool("ProduktInkrementCheck", true);
            }
        }
	}
}
