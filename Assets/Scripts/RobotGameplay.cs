﻿using Assets.GamePlay;
using Assets.GamePlay.Enums;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RobotGameplay : MonoBehaviour {

    private Animator anim;
    public GameObject panel = null;
    private bool isOnTrigger = false;
    private Text text = null;
    private MessagesForRobot messages;
    private Animator ownerAnim = null;
    private Animator scrumAnim = null;
    private Animator teamBau = null;
    private Animator teamKor = null;

    /// <summary>
    /// This Method will be once initialized
    /// </summary>
    void Start () {
        anim = GetComponent<Animator>();
        panel.SetActive(false);
        text = panel.transform.GetChild(0).GetComponentInChildren<Text>();
        messages = new MessagesForRobot();
    }

    /// <summary>
    /// If the player go to the roboter, an action will be performed depending on the game history
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            SetTeamAnimators();
            SetText();
            isOnTrigger = true;
            panel.SetActive(true);

            switch(BuildBungalow.Instance.GetGameStep())
            {
                case GameSteps.AufwandSchätzen:
                    SendPersonsToMeetingRoom(false);
                    break;
                case GameSteps.ReviewMeeting:
                case GameSteps.ProjektEndeSitzung:
                    SendPersonsToMeetingRoom(true);
                    break;
                case GameSteps.SprintErstellen:
                case GameSteps.SprintBacklogInAufgabenTeilenUndZuweisen:
                    BuildBungalow.Instance.MussZumRoboter = false;
                    SendPersonsToMeetingRoom(false);
                    break;
                case GameSteps.AkzeptanztestMitKunden:
                    BuildBungalow.Instance.MussZumRoboter = false;
                    LetCustomerCome(false);
                    break;
                case GameSteps.BauenEnde:
                    anim.SetInteger(BuildBungalow.ROBOT_ANIMATOR_STEP, (int)BuildBungalow.Instance.GetRobotStep());
                    BuildBungalow.Instance.SetGameStep(GameSteps.ReviewMeeting);
                    break;
                case GameSteps.SprintEnde:
                    if (BuildBungalow.Instance.GetSprintAblauf().SindSprintsFertig())
                        BuildBungalow.Instance.ProjektEnde();
                    else
                        BuildBungalow.Instance.SetGameStep(GameSteps.SprintBacklogInAufgabenTeilenUndZuweisen);
                    anim.SetInteger(BuildBungalow.ROBOT_ANIMATOR_STEP, (int)BuildBungalow.Instance.GetRobotStep());
                    break;
                case GameSteps.ProduktÜbergabe:
                    LetCustomerCome(true);
                    break;
                case GameSteps.DailyScrumMeeting:
                    SetDailyScrumMeeting();
                    break;
            }
        }
    }

    /// <summary>
    /// If the player go away, the panel will no longer displayed
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            isOnTrigger = false;
            panel.SetActive(false);
        }
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && isOnTrigger)
        {
            switch (BuildBungalow.Instance.GetRobotStep())
            {
                case RobotSteps.Start:
                    BuildBungalow.Instance.StarteSprintplansitzung();
                    anim.SetInteger(BuildBungalow.ROBOT_ANIMATOR_STEP, (int)BuildBungalow.Instance.GetRobotStep());
                    break;
                case RobotSteps.Sprintplansitzung:
                    if (BuildBungalow.Instance.GetGameStep() == GameSteps.SprintplansitzungEnde)
                    {
                        BuildBungalow.Instance.StarteSprint();
                        anim.SetInteger(BuildBungalow.ROBOT_ANIMATOR_STEP, (int)BuildBungalow.Instance.GetRobotStep());
                    }
                    break;
            }
        }
    }

    /// <summary>
    /// Set the text with days for the daily scrum Meeting and remove the ground for the bungalow
    /// </summary>
    private void SetDailyScrumMeeting()
    {
        SendPersonsToMeetingRoom(true);
        var sprintAblauf = BuildBungalow.Instance.GetSprintAblauf();
        text.text = "Sprint " + sprintAblauf.Sprint + " - Tag " + (sprintAblauf.Tag + 1) + "\n" + text.text;
        if (sprintAblauf.Tag == 0)
        { 
            try
            {
                var stiege = GameObject.Find("Stiege").transform.Find("Oberste").gameObject;
                stiege.SetActive(true);
                GameObject.FindGameObjectWithTag("GroundWegFürBungalow").SetActive(false);
            }
            catch (Exception) { }
        }
        BuildBungalow.Instance.MussZumRoboter = false;
    }

    /// <summary>
    /// Send the team to the meetingRoom with an animation
    /// </summary>
    /// <param name="allePersonen">false: without scrum muster, true: all persons</param>
    private void SendPersonsToMeetingRoom(bool allePersonen)
    {
        if (allePersonen)
            scrumAnim.SetBool("ToMeetingRaum", true);

        ownerAnim.SetBool("ToMeetingRaum", true);        
        teamBau.SetBool("ToMeetingRaum", true);
        teamKor.SetBool("ToMeetingRaum", true);
    }

    /// <summary>
    /// Let the customer come through the main door
    /// </summary>
    /// <param name="ownerAlso">true: owner went also to the place, false: only customer</param>
    private void LetCustomerCome(bool ownerAlso)
    {
        var mainDoor = GameObject.FindGameObjectWithTag("MainDoor").GetComponent<Animator>();
        mainDoor.SetBool("open", true);
        var kunde = GameObject.FindGameObjectWithTag("Kunde").GetComponent<Animator>();
        kunde.SetBool("Bungalow", true);

        if (ownerAlso)
            ownerAnim.SetBool("Bungalow", true);
    }

    /// <summary>
    /// Sets the animator for each team member
    /// </summary>
    private void SetTeamAnimators()
    {
        ownerAnim = GameObject.FindGameObjectWithTag("ProductOwner").GetComponentInChildren<Animator>();
        scrumAnim = GameObject.FindGameObjectWithTag("ScrumMaster").GetComponentInChildren<Animator>();
        teamBau = GameObject.FindGameObjectWithTag("TeamBauen").GetComponentInChildren<Animator>();
        teamKor = GameObject.FindGameObjectWithTag("TeamKorrigieren").GetComponentInChildren<Animator>();
    }

    /// <summary>
    /// Set the text of the panel
    /// </summary>
    private void SetText()
    {
        text.text = messages.GetMessage();
    }
}
