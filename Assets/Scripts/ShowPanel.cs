﻿using Assets.GamePlay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowPanel : MonoBehaviour {

    public GameObject panel = null;
	// Use this for initialization
	void Start () {
        panel.SetActive(false);
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && BuildBungalow.Instance.IsGameStarted())
        {
            var sprintPlansitzung = BuildBungalow.Instance.GetSprintplanSitzung();
            if (BuildBungalow.Instance.GetGameStep() == Assets.GamePlay.Enums.GameSteps.TeamEinteilen)
            {
                if (SetPanelActive(sprintPlansitzung))
                {
                    panel.SetActive(true);
                }
            }
            else if(panel.name.Equals("BacklogPanel") && BuildBungalow.Instance.GetGameStep() == Assets.GamePlay.Enums.GameSteps.ProduktBacklogKategorisieren)
            {
                panel.SetActive(true);
            }
            else if(SetPanelActive(sprintPlansitzung))
            {
                panel.SetActive(true);
            }
        }
    }

    private bool SetPanelActive(Sprintplansitzung sprintPlansitzung)
    {
        if((panel.name.Equals("AxelPanel") && !sprintPlansitzung.PersonAlreadySet("Axel")) || (panel.name.Equals("BerndPanel") && !sprintPlansitzung.PersonAlreadySet("Bernd")) || (panel.name.Equals("ChristinePanel") && !sprintPlansitzung.PersonAlreadySet("Christine")) || (panel.name.Equals("StefaniePanel") && !sprintPlansitzung.PersonAlreadySet("Stefanie")))
            return true;
        return false;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            panel.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update () {
        if((Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.M) || Input.GetKeyDown(KeyCode.T) || Input.GetKeyDown(KeyCode.K)) && (panel.name == "AxelPanel" || panel.name == "BerndPanel" || panel.name == "ChristinePanel" || panel.name == "StefaniePanel"))
        {
            panel.SetActive(false);
        }
	}
}
