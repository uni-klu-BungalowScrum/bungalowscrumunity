﻿using Assets.GamePlay;
using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartEndGame : MonoBehaviour {

    public Animator anim;
    public List<Animator> bungalowAnimations = null;
    public GameObject panel = null;
    public GameObject robot = null;
    public Material startMaterial = null;
    public Material endMaterial = null;
    private bool isOnTrigger = false;
    private Text text = null;
    // Use this for initialization
    void Start () {
        panel.SetActive(false);
        robot.SetActive(false);
        text = panel.transform.GetChild(0).GetComponentInChildren<Text>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            SetText();
            isOnTrigger = true;
            panel.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            isOnTrigger = false;
            panel.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.E) && isOnTrigger)
        {
            if (BuildBungalow.Instance.IsGameStarted())
            {
                anim.SetBool("play", false);
                BuildBungalow.Instance.EndGame();
                ResetAnimations();
                robot.SetActive(false);
                GetComponent<Renderer>().sharedMaterial = startMaterial;
                SetText();
            }
            else
            {
                GetComponent<Renderer>().sharedMaterial = endMaterial;
                anim.SetBool("play", true);
                BuildBungalow.Instance.StartGame();
                robot.SetActive(true);
                SetText();
            }
        }
    }

    private void SetText()
    {
        if (BuildBungalow.Instance.IsGameStarted())
            text.text = "'e': Spiel beenden";
        else
            text.text = "'e': Spiel beginnen";
    }

    /// <summary>
    /// Resets all animations which happens
    /// </summary>
    private void ResetAnimations()
    {
        var robotAnim = robot.GetComponent<Animator>();
        robotAnim.SetTrigger("New State");
        anim.SetBool("play", false);

        foreach (var bungalowItem in bungalowAnimations)
        {
            bungalowItem.SetBool("BaueAnforderung", false);
            bungalowItem.SetTrigger("New State");
        }

        var ownerAnim = GameObject.FindGameObjectWithTag("ProductOwner").GetComponent<Animator>();
        var scrumAnim = GameObject.FindGameObjectWithTag("ScrumMaster").GetComponent<Animator>();
        var bauenAnim = GameObject.FindGameObjectWithTag("TeamBauen").GetComponent<Animator>();
        var korrAnim = GameObject.FindGameObjectWithTag("TeamKorrigieren").GetComponent<Animator>();

        ownerAnim.SetInteger("Position", 0);
        ownerAnim.SetBool("ToMeetingRaum", false);
        ownerAnim.SetTrigger("New State");
        scrumAnim.SetInteger("Position", 0);
        scrumAnim.SetBool("ToMeetingRaum", false);
        scrumAnim.SetTrigger("New State");
        bauenAnim.SetInteger("Position", 0);
        bauenAnim.SetBool("ToMeetingRaum", false);
        bauenAnim.SetTrigger("New State");
        korrAnim.SetInteger("Position", 0);
        korrAnim.SetBool("ToMeetingRaum", false);
        korrAnim.SetTrigger("New State");
        
        var ground = GameObject.Find("GroundSpielfeld").transform.Find("BungalowGround").gameObject;
        ground.SetActive(true);
        var stiege = GameObject.Find("Stiege").transform.Find("Oberste").gameObject;
        stiege.SetActive(false);
    }
}
