﻿using Assets.GamePlay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Team : MonoBehaviour {
    public GameObject panel = null;
    private Text text = null;
    private bool isOnTrigger = false;
    public Animator anim = null;

	// Use this for initialization
	void Start () {
        panel.SetActive(false);
        text = panel.transform.GetChild(0).GetComponentInChildren<Text>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && BuildBungalow.Instance.GetGameStep() == Assets.GamePlay.Enums.GameSteps.Dokumentation)
        {
            text.text = "Drücken Sie 'e' damit das Team die Dokumentation schreibt!";
            panel.SetActive(true);
            isOnTrigger = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            panel.SetActive(false);
            isOnTrigger = false;
        }
    }

    // Update is called once per frame
    void Update () {
		if (isOnTrigger)
        {
            if (BuildBungalow.Instance.GetGameStep() == Assets.GamePlay.Enums.GameSteps.Dokumentation && Input.GetKeyDown(KeyCode.E))
            {
                panel.SetActive(false);
                anim.SetBool("ShowDoku", true);
                BuildBungalow.Instance.SetGameStep(Assets.GamePlay.Enums.GameSteps.ProduktÜbergabe);
            }
        }
	}
}
