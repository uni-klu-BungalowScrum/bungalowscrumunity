﻿using Assets.GamePlay;
using Assets.GamePlay.Anforderungen;
using Assets.GamePlay.Enums;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TeamBauen : MonoBehaviour {
    public List<Animator> anims = null;
    public GameObject panel = null;

    private Text text = null;
    private bool isOnTrigger = false;
    private MessagesForTeamBauen messages = new MessagesForTeamBauen();
    private SprintAblauf sprintAblauf = null;
    private bool buildAnforderung = false;
    // Use this for initialization
    void Start () {
        panel.SetActive(false);
        text = panel.transform.GetChild(0).GetComponentInChildren<Text>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && BuildBungalow.Instance.GetGameStep() == GameSteps.Bauen)
        {
            sprintAblauf = BuildBungalow.Instance.GetSprintAblauf();
            SetText();
            isOnTrigger = true;
            panel.SetActive(true);
        }
    }

    private void SetText()
    {
        text.text = messages.GetMessage(sprintAblauf.Tag-1);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (BuildBungalow.Instance.GetGameStep() == GameSteps.Bauen && buildAnforderung)
            {
                buildAnforderung = false;
                if (sprintAblauf.CurrentSprint.AnforderungenGebaut())
                    BuildBungalow.Instance.AnforderungenVonSprintGebaut();
                else
                {
                    //Only one task which is build faulty and will be corrected
                    if (sprintAblauf.Tag == 4)
                        BuildBungalow.Instance.SetGameStep(GameSteps.Korrigieren);
                    else
                        BuildBungalow.Instance.SetGameStep(GameSteps.DailyScrumMeeting);
                }
            }
            isOnTrigger = false;
            panel.SetActive(false);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (isOnTrigger && Input.GetKeyDown(KeyCode.E) && BuildBungalow.Instance.GetGameStep() == GameSteps.Bauen && buildAnforderung == false)
        {
            switch(sprintAblauf.Tag)
            {
                case 1:
                    sprintAblauf.BaueAnforderung(typeof(Keller), BuildBungalow.KELLER_NORDEN);
                    break;
                case 2:
                    sprintAblauf.BaueAnforderung(typeof(Keller), BuildBungalow.KELLER_OSTEN);
                    break;
                case 3:
                    sprintAblauf.BaueAnforderung(typeof(Keller), BuildBungalow.KELLER_SÜDEN);
                    break;
                case 4:
                    sprintAblauf.BaueAnforderungFehlerhaft(typeof(Keller), BuildBungalow.KELLER_WESTEN);
                    break;
                case 5:
                    sprintAblauf.BaueAnforderung(typeof(Keller), BuildBungalow.KELLERDECKE);
                    break;
                case 6:
                    sprintAblauf.BaueAnforderung(typeof(Erdgeschoss), BuildBungalow.ERDGESCHOSS_NO);
                    break;
                case 7:
                    sprintAblauf.BaueAnforderung(typeof(Erdgeschoss), BuildBungalow.ERDGESCHOSS_SW);
                    break;
                case 8:
                    sprintAblauf.BaueAnforderung(typeof(Erdgeschoss), BuildBungalow.ERDGESCHOSSDECKE);
                    break;
                case 9:
                    sprintAblauf.BaueAnforderung(typeof(Dach), BuildBungalow.DACH);
                    break;
            }
            anims[sprintAblauf.Tag - 1].SetBool("BaueAnforderung", true);
            panel.SetActive(false);
            buildAnforderung = true;
            BuildBungalow.Instance.MussZumRoboter = true;
        }
    }
}
