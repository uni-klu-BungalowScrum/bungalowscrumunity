﻿using Assets.GamePlay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Assets.GamePlay.Abstracts;
using Assets.GamePlay.Persons;
using Assets.GamePlay.Enums;

public class PersonenEinteilen : MonoBehaviour {

    public Animator axelAnim = null;
    public Animator berndAnim = null;
    public Animator christineAnim = null;
    public Animator stefanieAnim = null;
    public string personName = null;
    private bool isOnTrigger = false;
	// Use this for initialization
	void Start () {
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            isOnTrigger = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            isOnTrigger = false;
        }
    }

    // Update is called once per frame
    void Update () {
        if (BuildBungalow.Instance.GetGameStep() == GameSteps.TeamEinteilen && isOnTrigger && (Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.M) || Input.GetKeyDown(KeyCode.K) || Input.GetKeyDown(KeyCode.T)))
        {
            int position = 0;
            if (Input.GetKeyDown(KeyCode.P))
            {
                position = 1;
            }
            else if (Input.GetKeyDown(KeyCode.M))
            {
                position = 2;
            }
            else if (Input.GetKeyDown(KeyCode.T))
            {
                position = 3;
            }
            else if (Input.GetKeyDown(KeyCode.K))
            {
                position = 4;
            }

            Sprintplansitzung sitzung = BuildBungalow.Instance.GetSprintplanSitzung();

            switch (personName)
            {
                case "Axel":
                    if (sitzung.Axel_Position == 0)
                    { 
                        axelAnim.SetInteger("Position", position);
                        sitzung.Axel_Position = position;
                        AbstractPerson axel = new Axel();
                        axel.SetRolle((RolleEnum)position);
                        sitzung.SetPerson(axel);
                        SetTag(axelAnim, position);

                        if (sitzung.Bernd_Position == position)
                        {
                            sitzung.Bernd_Position = 0;
                            berndAnim.SetInteger("Position", 0);
                        }
                        if (sitzung.Christine_Position == position)
                        {
                            sitzung.Christine_Position = 0;
                            christineAnim.SetInteger("Position", 0);
                        }
                        if (sitzung.Stefanie_Position == position)
                        {
                            sitzung.Stefanie_Position = 0;
                            stefanieAnim.SetInteger("Position", 0);
                        }
                    }
                    break;
                case "Bernd":
                    if (sitzung.Bernd_Position == 0)
                    {
                        if (sitzung.Axel_Position == position)
                        {
                            axelAnim.SetInteger("Position", 0);
                            sitzung.Axel_Position = 0;
                        }

                        sitzung.Bernd_Position = position;
                        berndAnim.SetInteger("Position", position);
                        AbstractPerson bernd = new Bernd();
                        bernd.SetRolle((RolleEnum)position);
                        sitzung.SetPerson(bernd);
                        SetTag(berndAnim, position);

                        if (sitzung.Christine_Position == position)
                        {
                            sitzung.Christine_Position = 0;
                            christineAnim.SetInteger("Position", 0);
                        }
                        if (sitzung.Stefanie_Position == position)
                        {
                            sitzung.Stefanie_Position = 0;
                            stefanieAnim.SetInteger("Position", 0);
                        }
                    }
                    break;
                case "Christine":
                    if (sitzung.Christine_Position == 0)
                    {
                        if (sitzung.Axel_Position == position)
                        {
                            axelAnim.SetInteger("Position", 0);
                            sitzung.Axel_Position = 0;
                        }
                        if (sitzung.Bernd_Position == position)
                        {
                            sitzung.Bernd_Position = 0;
                            berndAnim.SetInteger("Position", 0);
                        }

                        sitzung.Christine_Position = position;
                        christineAnim.SetInteger("Position", position);
                        AbstractPerson christine = new Christine();
                        christine.SetRolle((RolleEnum)position);
                        sitzung.SetPerson(christine);
                        SetTag(christineAnim, position);

                        if (sitzung.Stefanie_Position == position)
                        {
                            sitzung.Stefanie_Position = 0;
                            stefanieAnim.SetInteger("Position", 0);
                        }
                    }
                    break;
                case "Stefanie":
                    if (sitzung.Stefanie_Position == 0)
                    {
                        if (sitzung.Axel_Position == position)
                        {
                            axelAnim.SetInteger("Position", 0);
                            sitzung.Axel_Position = 0;
                        }
                        if (sitzung.Bernd_Position == position)
                        {
                            sitzung.Bernd_Position = 0;
                            berndAnim.SetInteger("Position", 0);
                        }
                        if (sitzung.Christine_Position == position)
                        {
                            sitzung.Christine_Position = 0;
                            christineAnim.SetInteger("Position", 0);
                        }

                        sitzung.Stefanie_Position = position;
                        stefanieAnim.SetInteger("Position", position);
                        AbstractPerson stefanie = new Stefanie();
                        stefanie.SetRolle((RolleEnum)position);
                        sitzung.SetPerson(stefanie);
                        SetTag(stefanieAnim, position);
                    }
                    break;
            }

            BuildBungalow.Instance.AllePersonenZugeteilt();
        }
    }

    public void SetTag(Animator anim, int position)
    {
        if (position == 1)
            anim.tag = "ProductOwner";
        if (position == 2)
            anim.tag = "ScrumMaster";
        if (position == 3)
            anim.tag = "TeamBauen";
        if (position == 4)
            anim.tag = "TeamKorrigieren";
    }
}

