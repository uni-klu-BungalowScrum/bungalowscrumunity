﻿using Assets.GamePlay;
using Assets.GamePlay.Anforderungen;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TeamKorrigieren : MonoBehaviour {

    public Animator anim = null;
    public GameObject panel = null;
    private Text text = null; 
    private bool isOnTrigger = false;
	// Use this for initialization
	void Start () {
        text = panel.GetComponentInChildren<Text>();
        panel.SetActive(false);
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            text.text = "Drücken Sie 'e' um die Kellerwand Westen zu korrigieren";
            isOnTrigger = true;
            if (BuildBungalow.Instance.GetGameStep() == Assets.GamePlay.Enums.GameSteps.Korrigieren)
            {
                panel.SetActive(true);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            isOnTrigger = false;
            panel.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update () {
        if (isOnTrigger && BuildBungalow.Instance.GetGameStep() == Assets.GamePlay.Enums.GameSteps.Korrigieren && Input.GetKeyDown(KeyCode.E))
        {
            anim.SetBool("Korrigiere", true);
            panel.SetActive(false);
            var sprintAblauf = BuildBungalow.Instance.GetSprintAblauf();
            sprintAblauf.KorrigiereAnforderung(typeof(Keller), BuildBungalow.KELLER_WESTEN);
            BuildBungalow.Instance.SetGameStep(Assets.GamePlay.Enums.GameSteps.DailyScrumMeeting);
        }
	}
}
