﻿using Assets.GamePlay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Übergabe : MonoBehaviour {
    public GameObject panel = null;
	// Use this for initialization
	void Start () {
        panel.SetActive(false);
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && BuildBungalow.Instance.GetGameStep() == Assets.GamePlay.Enums.GameSteps.ProduktÜbergabe)
        {
            panel.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player" && BuildBungalow.Instance.GetGameStep() == Assets.GamePlay.Enums.GameSteps.ProduktÜbergabe)
        {
            BuildBungalow.Instance.GameEnde();
            panel.SetActive(false);
            var productOwner = GameObject.FindGameObjectWithTag("ProductOwner").GetComponent<Animator>();
            productOwner.SetBool("Bungalow", false);
            var kunde = GameObject.FindGameObjectWithTag("Kunde").GetComponent<Animator>();
            kunde.SetBool("Bungalow", false);
            var robot = GameObject.FindGameObjectWithTag("Robot").GetComponent<Animator>();
            robot.SetInteger(BuildBungalow.ROBOT_ANIMATOR_STEP, (int)BuildBungalow.Instance.GetRobotStep());
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
